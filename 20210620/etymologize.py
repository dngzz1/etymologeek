from bs4 import BeautifulSoup
from requests.models import codes
from load_wiki import load_wiki
from languageslist import languages_by_code, codes_by_language
import re # for string split
import myregex
import json

# Create etymology data.
# This will be a list of distinct etymologies, each being a dictionary of the form
# {
# "id": 6,
# "label": "a-",
# "iso" : "ang",
# "lang" : "Old English",
# "descendants": [4],
# "pos" : "",
# "gloss" : "Old English prefix used for forming words with the sense from, away, off,..."
# "realancestors" : []
# }

# I think I shouldn't chase through all the links because
# each etymologeek page should correspond to the information available from the
# corresponding wiktionary page. Tidier this way.

def load_etymologies(text):
    # The input is an html text of a specific word in a specific language.
    # The output is a list of etymologies [Etymology 1, Etymology 2, etc.]
    soup = BeautifulSoup(text, 'html.parser')
    sections = soup.find_all('h3')
    sections = [s for s in sections if re.match('Etymology*',s.span['id'])]
    results = []

    for section in sections:
        result = ""
        for tag in section.next_siblings:
            if tag.name == 'h3' or tag.name == 'h4':
                break 
            else:
                result += str(tag)
        results.append(result)
    return results
    





def create_trees_by_lang(word,lang):
    wiki = load_wiki(word,lang)
    etymologies = load_etymologies(wiki)
    trees = []
    for etymology in etymologies:
        tree = []
        nheight = 0
        id = 0
        # create entry for original word.
        iso = codes_by_language[lang] if lang in codes_by_language else "NOT FOUND"
        basedict = {
            "id": id,
            "label": word,
            "iso" : iso,
            "lang": lang,
            "descendants": [],
            }
        tree.append(basedict)
        # find the heights of leaves in the tree.
        etymology = myregex.remove_all_junk(etymology)
        heights = re.split("from",etymology,flags=re.IGNORECASE)
        heights = [h for h in heights if re.search("(Latn mention|Latinx mention)",h)]
        # heights = [myregex.remove_all_junk(h) for h in heights]
        heightdata = [[0]] # e.g. [[0],[1,2],[3]] for a diamond-shaped tree. Will append stuff to this list.
        for height in heights:
            nheight = nheight + 1
            current_height = [] # to be appended by all leaves in this height
            soup = BeautifulSoup(height,'html.parser')
            leaves_raw = soup.find_all(True,{'class':['Latn mention', 'Latinx mention']})
            for leaf_raw in leaves_raw:
                id = id + 1
                current_height.append(id)
                label = leaf_raw.text.strip()
                iso = leaf_raw.attrs['lang']
                lang = languages_by_code[iso] if iso in languages_by_code else 'NOT FOUND'
                descendants = sum(heightdata[:nheight], []) # concatenate list of lists
                leaf_dict =  {
                    "id": id, 
                    "label": label, 
                    "iso": iso, 
                    "lang": lang,
                    "descendants": descendants,
                    }
                tree.append(leaf_dict)
            heightdata.append(current_height)
        trees.append(tree)
    return trees




# TESTING REGION.
# I have tested
# cucumber
# tomato
# bear
# discipline
# much
# old

# So être,pouvoir still have problems.
# word = 'bear'
# lang = 'English'
# trees = create_trees_by_lang(word,lang)
# print(json.dumps(trees))
