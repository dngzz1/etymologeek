from listify import listify
from load_wiki import load_wiki
from languageslist import languages_by_code
from etymologize import create_trees_by_lang
import os
import json

entries = listify('entries.txt')
# entries = entries[10:20]

# update entries.
new_entries = []
count = 0


for entry in entries:
    count = count + 1
    print(count)
    id = entry[0]
    iso = entry[1]
    lang = languages_by_code[iso]
    word = entry[2]
    trees = create_trees_by_lang(word,lang)
    new_entry = [id,iso,word,trees]
    new_entries.append(new_entry)


# Write to file
filename = 'new_entries.txt'

if os.path.exists(filename):
    append_write = 'w' # overwrite if already exists
else:
    append_write = 'w' # make a new file if not

f = open(filename,append_write, encoding='utf8')
for entry in new_entries:
    f.write(json.dumps(entry, ensure_ascii=False))
    f.write('\n\n')
f.close()
