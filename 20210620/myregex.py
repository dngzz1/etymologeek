# Extra functions for regex.
import re 

def remove_all_junk(text):
    text = re.sub("Akin to.*?\.","",text)
    text = re.sub("Cognate with.*?\.","",text)
    text = re.sub("\(compare.*?\)", "", text)
    text = re.sub("Compare also.*?\.","",text)
    text = re.sub("Doublet of.*?\.","",text)
    text = re.sub("Reinforced by.*?\.","",text)
    text = re.sub("Related to.*?\.","",text)
    text = re.sub("see also.*?\.","",text,flags=re.IGNORECASE)
    return text
