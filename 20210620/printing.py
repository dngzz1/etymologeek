# Helper function to print stuff nicely.
import json

# this one prints a list nicely.
def json_printing(json_list):
    json_string = json.dumps(json_list)
    parsed = json.loads(json_string)
    print(json.dumps(parsed, indent=4, sort_keys=True))
