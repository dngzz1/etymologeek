# Converts Linas' txt into a list of words to be etymologized.

# 1. MAIN FUNCTION.
def listify(filename):
    lines = []
    with open(filename) as f:
        lines = f.readlines()

    # Print result.
    def pretty_print(lines):
        count = 0
        for line in lines:
            count += 1
            print(f'line {count}: {line}')    


    entries = []
    for line in lines:
        l = line.split('[',1)[0] # (158877, 'eng', 'martyrsome', 
        l = l[1:] # 158877, 'eng', 'martyrsome', 
        word_id = int(l.split(',',1)[0])
        l = l[l.index("'")+1:] # eng', 'martyrsome', 
        language_code = l[:l.index("'")]
        l = l[l.index(",")+1:] # , 'martyrsome', 
        l = l[l.index("'")+1:] # 'martyrsome', 
        word = l[:l.index("'")]
        entries.append((word_id, language_code,word))
    return(entries)


# 2. HELPER FUNCTIONS.
# none


# 3. RUN PROGRAM.
# entries = listify('entries.txt')
# for entry in entries:
#     print(entry)