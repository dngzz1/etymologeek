import urllib.parse
import requests
import re # for string split
from bs4 import BeautifulSoup, Comment
# ~/Documents/coding/etymologeek/2021-06





# 1. MAIN FUNCTION.
def load_wiki(word,language):
    html_doc = get_plaintext(word)
    if html_doc == '': # word not found
        return ''
    soup = BeautifulSoup(html_doc, 'html.parser')
    # Find all h2 with id being the target language.
    removeComments(soup)
    sections = soup.find_all('h2')
    hasFound = False
    for section in sections:
        if section.span['id'] == language:
            tag = section
            hasFound = True
            break
    # If no such language exists, return empty string.
    if not hasFound:
        print('Word <' + word + '> exists but language <' + language + '> entry NOT FOUND.')
        return ''
    # Language found, continue
    result = ""
    for tag in section.next_siblings:
        if tag.name == 'h2':
            break
        else:
            result += str(tag)
    print('Word <' + word + '> in language <' + language + '> FOUND.')
    return(result)


# 2. HELPER FUNCTIONS.
def get_plaintext(keyword):
    # Enter a keyword (what you type in Wiktionary) and this function
    # will return the webpage in plaintext.
    # https://stackoverflow.com/questions/52351081/extracting-etymological-information-from-wiktionary
    try:
        main_api = 'https://en.wiktionary.org/w/api.php?format=json&action=query&prop=extracts&'
        url = main_api + urllib.parse.urlencode({'titles': keyword})
        # the urlencode will deal with turning white spaces into %20 etc.
        # url will be a string of the url.

        json_data = requests.get(url).json()
        json_data = json_data['query']['pages']
        # the following line is a hack to get the first (and only) entry which is a number that changes every time.
        # there should be a better method.
        json_data = json_data[list(json_data.keys())[0]]
        html_doc = json_data['extract']

        return html_doc
    except:
        print('Word <' + keyword + '> NOT FOUND.')
        return ''

def removeComments(soup):
    comments = soup.findAll(text=lambda text:isinstance(text,Comment))
    [comment.extract() for comment in comments] 


# 3. RUN MAIN FUNCTION.
# print(load_wiki('lapin','French'))



