# Documentation

## Updates
### 2021-07-21
Run main.py.
It reads entries.txt and outputs new_entries.txt.
The etymology data is processed as follows.
If the etymology is of the form
"From a, from b, c, from d, from e,"
then there should be no problem generating the dictionary format.
There is a problem when there is too much junk text, e.g.
for French être and pouvoir.

### 2021-06-26
Folder contains python files
listify.py, load\_wiki.py, main.py
and text files 
entries and new\_entries.txt

Run main.py.
It uses listify.py to read entries.txt to see which words to scrape.
Then it will use load\_wiki.py to get the html text.
It can only do English so I have filtered out the non-English entries.
Right now it can't do any etymology; it will just have the html as its new data.

Problems: some entries cannot be found. These ones have blank updated entries.
Question: is there anywhere I can get a table between language and language code? e.g. English - en - eng.

